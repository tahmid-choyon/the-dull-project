package selectionsort;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        // arrays to be sorted (ascending)
        int test_small_array[] = {9999, 102, 74, 14, 12, 10, 10, 8, 5, 5, 2, 2, 0};
        int test_large_array[] = {9999, 102, 74, 14, 12, 10, 10, 8, 5, 5, 2, 2, 0, 2589, 741, 56987, 124578, 123456, 657, 157, 3574, 1257};

        selectionSort(test_small_array);
        System.out.println("Small Array: " + Arrays.toString(test_small_array));

        selectionSort(test_large_array);
        System.out.println("Large Array: " + Arrays.toString(test_large_array));
    }

    public static void selectionSort(int[] array) {

        int array_length = array.length;

        for (int i = 0; i < array_length - 1; i++) {

            int lowest_number_index = i;

            for (int j = i + 1; j < array_length; j++) {
                if (array[j] < array[lowest_number_index]) {
                    lowest_number_index = j;
                }
            }

            //swap
            int temp = array[i];
            array[i] = array[lowest_number_index];
            array[lowest_number_index] = temp;
        }
    }
}
