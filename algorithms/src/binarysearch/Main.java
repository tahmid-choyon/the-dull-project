public class Main {
    public static void main(String[] args) {
        int sortedArray[] = {1, 2, 3, 5, 7, 10, 125, 5000, 99999};
        int numbersToFind[] = {2, 120, 200, 5, 99999, 8};

        for (int num : numbersToFind) {
            int index = binarySearch(num, sortedArray, 0, sortedArray.length - 1);
            if (index != -1) {
                System.out.println(num + " is at index [" + index + "]");
            } else {
                System.out.println(num + " is not found");
            }
        }
    }

    public static int binarySearch(int numberToFind, int[] sortedArray, int lower, int upper) {
        int mid = (lower + upper) / 2;

        if (sortedArray[mid] == numberToFind)
            return mid;
        else if (lower >= upper)
            return -1;
        else if (sortedArray[mid] < numberToFind)
            lower = mid + 1;
        else if (sortedArray[mid] > numberToFind)
            upper = mid - 1;
        return binarySearch(numberToFind, sortedArray, lower, upper);
    }
}