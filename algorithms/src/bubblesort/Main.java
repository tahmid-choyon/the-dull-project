package bubblesort;
/*
Source: http://www.geeksforgeeks.org/bubble-sort/

Bubble Sort is the simplest sorting algorithm that works by repeatedly swapping the adjacent elements if they are in wrong order.

Example:
First Pass:
( 5 1 4 2 8 ) –> ( 1 5 4 2 8 ), Here, algorithm compares the first two elements, and swaps since 5 > 1.
( 1 5 4 2 8 ) –>  ( 1 4 5 2 8 ), Swap since 5 > 4
( 1 4 5 2 8 ) –>  ( 1 4 2 5 8 ), Swap since 5 > 2
( 1 4 2 5 8 ) –> ( 1 4 2 5 8 ), Now, since these elements are already in order (8 > 5), algorithm does not swap them.

Second Pass:
( 1 4 2 5 8 ) –> ( 1 4 2 5 8 )
( 1 4 2 5 8 ) –> ( 1 2 4 5 8 ), Swap since 4 > 2
( 1 2 4 5 8 ) –> ( 1 2 4 5 8 )
( 1 2 4 5 8 ) –>  ( 1 2 4 5 8 )
Now, the array is already sorted, but our algorithm does not know if it is completed. The algorithm needs one whole pass without any swap to know it is sorted.

Third Pass:
( 1 2 4 5 8 ) –> ( 1 2 4 5 8 )
( 1 2 4 5 8 ) –> ( 1 2 4 5 8 )
( 1 2 4 5 8 ) –> ( 1 2 4 5 8 )
( 1 2 4 5 8 ) –> ( 1 2 4 5 8 )
 */

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {

        // arrays to be sorted (ascending)
        int test_small_array[] = {9999, 102, 74, 14, 12, 10, 10, 8, 5, 5, 2, 2, 0};
        int test_large_array[] = {5, 12, 14, 74, 10, 0, 2, 5, 2, 8, 10, 102, 1234, 2589, 741, 56987, 124578, 123456, 657, 157, 3574, 1257};

        bubbleSort(test_small_array);
        System.out.println("Small Array: " + Arrays.toString(test_small_array));

        bubbleSort(test_large_array);
        System.out.println("Large Array: " + Arrays.toString(test_large_array));
    }

    public static void bubbleSort(int[] array) {

        int array_length = array.length;

        for (int i = 0; i < array_length - 1; i++) {
            for (int j = 0; j < array_length - i - 1; j++) {

                if (array[j] > array[j + 1]) {
                    // swap
                    int temp = array[j];
                    array[j] = array[j + 1];
                    array[j + 1] = temp;
                }
            }
        }
    }
}
