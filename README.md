# the-dull-project
Implementing Data Structures and Algorithms using [Java](#java) &amp;&amp; [Python](#python)

## Java
| Sorting |
| ----- |
| [Bubble Sort](https://github.com/tahmid-choyon/the-dull-project/blob/master/algorithms/src/bubblesort/Main.java) |
| [Selection Sort](https://github.com/tahmid-choyon/the-dull-project/blob/master/algorithms/src/selectionsort/Main.java) |


|Searching|
| ----- |
|[BinarySearch](https://github.com/tahmid-choyon/the-dull-project/blob/master/algorithms/src/binarysearch/Main.java)|


## Python
